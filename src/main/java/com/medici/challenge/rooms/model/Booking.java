package com.medici.challenge.rooms.model;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

public class Booking {
    private final UUID id;
    @NotNull
    private final UUID roomId;
    @NotNull
    private final Date fromDate;
    @NotNull
    private final Date toDate;
    
    public Booking(UUID id, UUID roomId, Date fromDate, Date toDate) {
        this.id = id;
        this.roomId = roomId;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }
    
    public UUID getId() {
        return id;
    }

    public UUID getRoomId() {
        return roomId;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

}
