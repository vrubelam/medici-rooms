package com.medici.challenge.rooms.validator;

import java.util.Date;

public class DateValidator {

    public static boolean isFromPriorToTo(Date from, Date to) {
        return from.before(to);
    }

    public static boolean isDateInRange(Date date, Date from, Date to) {
        return !(date.before(from) || date.after(to));
    }
}
