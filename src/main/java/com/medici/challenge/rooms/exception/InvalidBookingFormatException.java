package com.medici.challenge.rooms.exception;

public class InvalidBookingFormatException extends Throwable {

    public InvalidBookingFormatException(String message) {
        super(message);
    }
}
