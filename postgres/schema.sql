CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

\connect roomsdb

CREATE TABLE public.rooms (
    id UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    roomType VARCHAR(10) NOT NULL
);

CREATE TABLE public.bookings (
    id UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    roomId UUID REFERENCES public.rooms(id),
    fromDate DATE,
    toDate DATE
);

CREATE TABLE public.deleted (
    id UUID NOT NULL PRIMARY KEY,
    roomID UUID REFERENCES public.rooms(id),
    fromDate DATE,
    toDate DATE
);

INSERT INTO public.rooms (roomType) VALUES ('single'),('single'),('double'),('double'),('suite')