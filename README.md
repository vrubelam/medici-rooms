# Medici Group - Technical Challenge

## Description
The task at hand is to create a small microservice for a room booking service. The service will allow users to list the available rooms. Make a booking and cancel a booking.
The service will talk to a PostgreSQL database, and has to be written in Java (using any framework or library, although Spring Boot is recommended).
The task has to be delivered in a single git repository. The service has to contain a Dockerfile to run inside Docker and also docker-compose file to launch the service and the database.
The code has to be as production grade as possible. Optionally, tests can be included, although this is not a requirement.
There is a two weeks time limit for the task.

## Getting started
1. Build the project.
2. run `docker-compose`

## Endpoints
`GET /rooms` Get all rooms 

`POST /booking` Create new booking. Request body should be as follows:

```
{
    roomId: ${roomId},
    fromDate: ${fromDate},
    toDate: ${toDate}
}
```

`GET /bookings` Get a list of bookings for all rooms

`DELETE /bookings/${bookingId}` Delete/Cancel a booking

`GET /room/${roomId}` Get room by id