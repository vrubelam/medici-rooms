package com.medici.challenge.rooms.dao;

import com.medici.challenge.rooms.exception.InvalidBookingFormatException;
import com.medici.challenge.rooms.model.Booking;

import java.util.List;
import java.util.UUID;

public interface BookingDao {

    boolean createBooking(UUID id, Booking booking) throws InvalidBookingFormatException;

    default boolean createBooking(Booking booking) throws InvalidBookingFormatException {
        UUID id = UUID.randomUUID();
        return createBooking(id, booking);
    }

    boolean deleteBooking(UUID id);

    List<Booking> getAllBookings();
}
