package com.medici.challenge.rooms.constants;

public final class ResponseConstants {
    public static final int OK = 2000;
    public static final int ERROR = 4100;

    public static final String OK_MESSAGE = "Success";
    public static final String NOT_FOUND = "Not found";
    public static final String CREATED = "Booking created";
    public static final String NOT_CREATED = "Could not create booking";
    public static final String DELETED = "Booking deleted";
    public static final String NOT_DELETED = "Could not delete booking";
}
