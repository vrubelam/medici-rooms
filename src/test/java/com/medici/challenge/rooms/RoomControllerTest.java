package com.medici.challenge.rooms;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import com.medici.challenge.rooms.api.RoomController;
import com.medici.challenge.rooms.model.Booking;
import com.medici.challenge.rooms.model.Room;
import com.medici.challenge.rooms.response.StringResponse;
import com.medici.challenge.rooms.service.RoomService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

@ExtendWith(MockitoExtension.class)
public class RoomControllerTest {

    @InjectMocks
    RoomController roomController;

    @Mock
    RoomService roomService;

    @Test
    public void testGetRoom() {

        Room room = new Room(UUID.randomUUID(), "suite");
        UUID id = room.getId();
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        when(roomService.getRoom(id)).thenReturn(room);
        StringResponse response = roomController.getRoom(id);
        assertThat(response.getRoom()).isNotNull();
        assertThat(response.getRoom().getId()).isEqualByComparingTo(id);
    }

    @Test
    public void testGetRooms() {
        Room[] rooms = new Room[] {
                new Room(UUID.randomUUID(), "single"),
                new Room(UUID.randomUUID(), "double"),
                new Room(UUID.randomUUID(), "suite")
        };
        try {
            Date d1 = new SimpleDateFormat("dd.MM.yyyy").parse("12.02.2019");
            Date d2 = new SimpleDateFormat("dd.MM.yyyy").parse("12.04.2019");
            Booking booking = new Booking(UUID.randomUUID(), rooms[0].getId(), d1, d2);
            rooms[0].getBookingCalendar().add(booking);
        } catch (ParseException ex) {
            // Should always parse, so do nothing
        }
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        when(roomService.getAllRooms()).thenReturn(Arrays.asList(rooms));
        StringResponse response = roomController.getRooms();
        assertThat(response.getRooms().isEmpty()).isFalse();
        assertThat(response.getRooms().size()).isEqualTo(3);
        assertThat(response.getRooms().get(0).getBookingCalendar().size()).isEqualTo(1);

    }
}
