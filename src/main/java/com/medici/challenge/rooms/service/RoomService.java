package com.medici.challenge.rooms.service;

import com.medici.challenge.rooms.dao.RoomDao;
import com.medici.challenge.rooms.model.Room;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class RoomService {
    
    private final RoomDao roomDao;
    
    @Autowired
    public RoomService(@Qualifier("room_postgres") RoomDao roomDao) {
        this.roomDao = roomDao;
    }
    
    public List<Room> getAllRooms() {
        return roomDao.getAllRooms();
    }

    public Room getRoom(UUID id) {
        return roomDao.getRoom(id);
    }
}
