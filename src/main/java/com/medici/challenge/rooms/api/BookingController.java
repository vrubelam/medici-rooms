package com.medici.challenge.rooms.api;

import com.medici.challenge.rooms.constants.ResponseConstants;
import com.medici.challenge.rooms.exception.InvalidBookingFormatException;
import com.medici.challenge.rooms.model.Booking;
import com.medici.challenge.rooms.response.StringResponse;
import com.medici.challenge.rooms.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
public class BookingController {
    private final BookingService bookingService;

    @Autowired
    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @PostMapping(value = "/booking", produces = MediaType.APPLICATION_JSON_VALUE)
    public StringResponse createBooking(@Valid @RequestBody Booking booking) {
        try {
            if (bookingService.createBooking(booking)) {
                return new StringResponse(ResponseConstants.OK, ResponseConstants.CREATED);
            } else {
                return new StringResponse(ResponseConstants.ERROR, ResponseConstants.NOT_CREATED);
            }
        } catch (InvalidBookingFormatException ex) {
            return new StringResponse(ResponseConstants.ERROR, ex.getMessage());
        }
    }

    @DeleteMapping(path = "/bookings/{bookingId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public StringResponse deleteBooking(@PathVariable UUID bookingId) {
        if (bookingService.deleteBooking(bookingId)) {
            return new StringResponse(ResponseConstants.OK, ResponseConstants.DELETED);
        }
        return new StringResponse(ResponseConstants.ERROR, ResponseConstants.NOT_DELETED);
    }

    @GetMapping(path = "/bookings", produces = MediaType.APPLICATION_JSON_VALUE)
    public StringResponse getBookings() {
        List<Booking> bookings = bookingService.getAllBookings();
        StringResponse response = new StringResponse(ResponseConstants.OK, ResponseConstants.OK_MESSAGE);
        response.setBookings(bookings);
        return response;
    }
}
