package com.medici.challenge.rooms.dao;

import com.medici.challenge.rooms.model.Room;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface RoomDao {
    List<Room> getAllRooms();
    Room getRoom(UUID id);
}