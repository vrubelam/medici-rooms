FROM java:8-jdk-alpine

ARG JAR_FILE=target/medici-rooms.jar

ADD ${JAR_FILE} medici-rooms.jar

ENTRYPOINT ["java", "-jar", "medici-rooms.jar"]