package com.medici.challenge.rooms;

import com.medici.challenge.rooms.api.BookingController;
import com.medici.challenge.rooms.constants.ResponseConstants;
import com.medici.challenge.rooms.exception.InvalidBookingFormatException;
import com.medici.challenge.rooms.model.Booking;
import com.medici.challenge.rooms.response.StringResponse;
import com.medici.challenge.rooms.service.BookingService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.validation.constraints.Null;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.fail;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class BookingControllerTest {

    @InjectMocks
    BookingController bookingController;

    @Mock
    BookingService bookingService;

    @Test
    public void getBookings() {
        try {
            MockHttpServletRequest request = new MockHttpServletRequest();
            RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
            UUID roomId = UUID.randomUUID();
            Booking[] bookings = new Booking[] {
                    new Booking(UUID.randomUUID(), roomId, new SimpleDateFormat("dd.MM.yyyy").parse("14.01.2020"), new SimpleDateFormat("dd.MM.yyyy").parse("12.05.2020")),
                    new Booking(UUID.randomUUID(), roomId, new SimpleDateFormat("dd.MM.yyyy").parse("10.09.2020"), new SimpleDateFormat("dd.MM.yyyy").parse("12.10.2020"))
            };

            when(bookingService.getAllBookings()).thenReturn(Arrays.asList(bookings));

            StringResponse response = bookingController.getBookings();
            assertThat(response.getBookings().size()).isEqualTo(2);
            assertThat(response.getBookings().get(0).getRoomId()).isEqualByComparingTo(response.getBookings().get(1).getRoomId());
        } catch (ParseException ex) {
            // Should not throw the exception
        }

    }

    @Test
    public void testCreateBooking() {
        try {
            MockHttpServletRequest request = new MockHttpServletRequest();
            RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

            Booking booking = new Booking(UUID.randomUUID(),
                    UUID.randomUUID(),
                    new SimpleDateFormat("dd.MM.yyyy").parse("10.01.2020"),
                    new SimpleDateFormat("dd.MM.yyyy").parse("15.01.2020"));

            when(bookingService.createBooking(booking)).thenReturn(true);
            StringResponse response = bookingController.createBooking(booking);
            assertThat(response.getStatus()).isEqualTo(ResponseConstants.OK);


        } catch (ParseException ex1) {
            // Should not throw the exception
            fail("Parsing exception thrown");
        } catch (InvalidBookingFormatException ex2) {
            assert(ex2.getMessage()).equals("Exception thrown");
        }
    }

    @Test
    public void testDeleteBooking() {
        try {
            MockHttpServletRequest request = new MockHttpServletRequest();
            RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
            Booking booking = new Booking(UUID.randomUUID(),
                    UUID.randomUUID(),
                    new SimpleDateFormat("dd.MM.yyyy").parse("10.01.2020"),
                    new SimpleDateFormat("dd.MM.yyyy").parse("01.01.2020"));

            when(bookingService.deleteBooking(booking.getId())).thenReturn(true);
            StringResponse response = bookingController.deleteBooking(booking.getId());
            assertThat(response.getStatus()).isEqualTo(ResponseConstants.OK);
        } catch (ParseException ex) {
            fail("Parsing exception thrown");
        }
    }
}
