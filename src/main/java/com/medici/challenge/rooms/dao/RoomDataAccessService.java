package com.medici.challenge.rooms.dao;

import com.medici.challenge.rooms.model.Booking;
import com.medici.challenge.rooms.model.Room;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository("room_postgres")
public class RoomDataAccessService implements RoomDao {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public RoomDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Room> getAllRooms() {

        String statement = "SELECT r.id, r.roomType, b.id AS bookingId, b.roomId, b.fromDate, b.toDate FROM rooms r LEFT JOIN bookings b ON r.id = b.roomId";
        Map<UUID, List<Booking>> bookings = new HashMap<>();
        List<Room> rooms = jdbcTemplate.query(statement, (resultSet, i) -> {
            UUID roomId = UUID.fromString(resultSet.getString("id"));
            if (resultSet.getString("bookingId") != null) {
                UUID bookingId = UUID.fromString(resultSet.getString("bookingId"));
                if (bookings.containsKey(roomId)) {
                    bookings.get(roomId).add(new Booking(bookingId,
                            roomId,
                            resultSet.getDate("fromDate"),
                            resultSet.getDate("toDate")));
                } else {
                    List<Booking> b = new ArrayList<>();
                    b.add(new Booking(bookingId,
                            roomId,
                            resultSet.getDate("fromDate"),
                            resultSet.getDate("toDate")));
                    bookings.put(roomId, b);
                }
            }
            return new Room(UUID.fromString(resultSet.getString("id")), resultSet.getString("roomType"));
        });

        List<Room> distinct = rooms.stream().distinct().collect(Collectors.toList());
        distinct.forEach(room -> {
            if (bookings.containsKey(room.getId())) {
                room.getBookingCalendar().addAll(bookings.get(room.getId()));
            }
        });
        return distinct;
    }

    @Override
    public Room getRoom(UUID id) {
        String statement = "SELECT id, roomType FROM rooms WHERE id = ?";
        Object[] params = new Object[]{id};

        List<Room> rooms = jdbcTemplate.query(statement, params, (resultSet, i) -> {
            return new Room(UUID.fromString(resultSet.getString("id")), resultSet.getString("roomType"));
        });
        if (rooms.isEmpty()) {
            return null;
        }
        String queryForBookings = "SELECT id, roomId, fromDate, toDate FROM bookings WHERE roomId = ?";
        List<Booking> bookings = jdbcTemplate.query(queryForBookings, params, (resultSet, i) -> {
            return new Booking(UUID.fromString(resultSet.getString("id")), id, resultSet.getDate("fromDate"), resultSet.getDate("toDate"));
        });
        Room room = rooms.get(0);
        room.getBookingCalendar().addAll(bookings);
        return room;
    }

}
