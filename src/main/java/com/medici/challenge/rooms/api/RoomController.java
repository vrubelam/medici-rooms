package com.medici.challenge.rooms.api;

import com.medici.challenge.rooms.constants.ResponseConstants;
import com.medici.challenge.rooms.model.Room;
import com.medici.challenge.rooms.response.StringResponse;
import com.medici.challenge.rooms.service.RoomService;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RoomController {

    private final RoomService roomService;
    
    @Autowired
    public RoomController(RoomService roomService) {
        this.roomService = roomService;
    }
    @GetMapping(path = "/rooms", produces = MediaType.APPLICATION_JSON_VALUE)
    public StringResponse getRooms() {
        StringResponse response = new StringResponse(ResponseConstants.OK, ResponseConstants.OK_MESSAGE);
        response.setRooms(roomService.getAllRooms());
        return response;
    }

    @GetMapping(path = "/room/{roomId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public StringResponse getRoom(@PathVariable UUID roomId) {
        Room room = roomService.getRoom(roomId);
        if (room != null) {
            StringResponse response = new StringResponse(ResponseConstants.OK, ResponseConstants.OK_MESSAGE);
            response.setRoom(room);
            return response;
        }
        return new StringResponse(ResponseConstants.ERROR, ResponseConstants.NOT_FOUND);
    }
}
