package com.medici.challenge.rooms.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Room {
    private final UUID id;
    private final String roomType;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final List<Booking> bookingCalendar;

    public Room(UUID id, String type) {
        this.id = id;
        this.roomType = type;
        this.bookingCalendar = new ArrayList<>();
    }

    public UUID getId() {
        return id;
    }

    public String getRoomType() {
        return roomType;
    }

    public void addBooking(Booking booking) {
        this.bookingCalendar.add(booking);
    }
    public List<Booking> getBookingCalendar() {
        return bookingCalendar;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        Room room = (Room) obj;
        return room.getId().equals(((Room) obj).getId());
    }

    @Override
    public int hashCode() {
        return this.getId().hashCode();
    }
}
