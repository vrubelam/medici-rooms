package com.medici.challenge.rooms.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.medici.challenge.rooms.model.Booking;
import com.medici.challenge.rooms.model.Room;

import java.util.List;

public class StringResponse {

    private final int status;
    private final String message;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Room room;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<Room> rooms;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Booking booking;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<Booking> bookings;

    public StringResponse(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    public List<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }
}
