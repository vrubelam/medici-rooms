package com.medici.challenge.rooms.service;

import com.medici.challenge.rooms.dao.BookingDao;
import com.medici.challenge.rooms.exception.InvalidBookingFormatException;
import com.medici.challenge.rooms.model.Booking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class BookingService {

    private final BookingDao bookingDao;

    @Autowired
    public BookingService(@Qualifier("booking_postgres") BookingDao bookingDao) {
        this.bookingDao = bookingDao;
    }

    public boolean createBooking (Booking booking) throws InvalidBookingFormatException {
        return bookingDao.createBooking(booking);
    }

    public boolean deleteBooking(UUID bookingId) {
        return bookingDao.deleteBooking(bookingId);
    }

    public List<Booking> getAllBookings() {
        return bookingDao.getAllBookings();
    }
}
