package com.medici.challenge.rooms.dao;

import com.medici.challenge.rooms.exception.InvalidBookingFormatException;
import com.medici.challenge.rooms.model.Booking;
import com.medici.challenge.rooms.validator.DateValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Types;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository("booking_postgres")
public class BookingDataAccessService implements BookingDao {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public BookingDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public boolean createBooking(UUID id, Booking booking)  throws InvalidBookingFormatException {
        if (!DateValidator.isFromPriorToTo(booking.getFromDate(), booking.getToDate())) {
            throw new InvalidBookingFormatException("fromDate must be a date before toDate");
        }
        String select = "SELECT id, roomId, fromDate, toDate FROM bookings WHERE roomId = ?";
        Object[] selectParams = new Object[] { booking.getRoomId() };
        int[] selectTypes = new int[] { Types.OTHER };

        List<Booking> bookings = jdbcTemplate.query(select, selectParams, selectTypes, (resultSet, i) -> {
            UUID bookingId = UUID.fromString(resultSet.getString("id"));
            UUID roomId = UUID.fromString(resultSet.getString("roomId"));
            return new Booking(bookingId,
                    roomId,
                    resultSet.getDate("fromDate"),
                    resultSet.getDate("toDate"));
        });

        boolean bookingExits = bookings.stream()
                .anyMatch(b -> DateValidator.isDateInRange(booking.getFromDate(), b.getFromDate(), b.getToDate()) || DateValidator.isDateInRange(booking.getToDate(), b.getFromDate(), b.getToDate()));

        if (!bookingExits) {
            String statement = "INSERT INTO bookings (id, roomId, fromDate, toDate) VALUES (?, ?, ?, ?)";
            Object[] params = new  Object[] { id, booking.getRoomId(), booking.getFromDate(), booking.getToDate() };
            int[] types = new int[] { Types.OTHER, Types.OTHER, Types.DATE, Types.DATE };
            return jdbcTemplate.update(statement, params, types) == 1;
        }

        return false;
    }

    @Override
    public boolean deleteBooking(UUID id) {
        // TODO: This would probably be better if called as an SQL procedure
        String statement = "INSERT INTO deleted (id, roomId, fromDate, toDate) SELECT id, roomId, fromDate, toDate FROM bookings WHERE id = ?; DELETE FROM bookings WHERE id = ?";
        Object[] params = new Object[]{id, id};
        return jdbcTemplate.update(statement, params) == 1;
    }

    @Override
    public List<Booking> getAllBookings() {
        String statement = "SELECT id, roomId, fromDate, toDate FROM bookings";
        return jdbcTemplate.query(statement, (resultSet, i) -> new Booking(
                    UUID.fromString(resultSet.getString("id")),
                    UUID.fromString(resultSet.getString("roomId")),
                    resultSet.getDate("fromDate"),
                    resultSet.getDate("toDate")));
    }
}
